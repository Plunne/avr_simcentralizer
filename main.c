#define F_CPU 16000000UL

#include "Setup.h"

int main(void)
{
    /* Init */
    InitRegisters();
    
    /* Program */
    while (1) {

        setLOW(PORTD, TX_LED);
        setLOW(PORTB, RX_LED);
        displayGear(getGear());
    }
}
