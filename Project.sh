#!/bin/sh

$EDITOR main.c \
    Setup.h \
    InitRegisters.c \
    Gear.h \
    GearDisplay.c \
    GearShifter.c
