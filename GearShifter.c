#include "Gear.h"

unsigned char getGear(void) {

    unsigned char gear;
    unsigned short x = analogRead(&PINF, SHIFTER_X);
    unsigned short y = analogRead(&PINF, SHIFTER_Y);

    // Gear 1,3,5
    if (y < AXIS_CENTER_NEG) {
        if (x < AXIS_CENTER_NEG) gear = 1;
        else if (x > AXIS_CENTER_POS) gear = 5;
        else gear = 3;
    // Gear 2,4,6
    } else if (y > AXIS_CENTER_POS) {
        if (x < AXIS_CENTER_NEG) gear = 2;
        else if (x > AXIS_CENTER_POS) gear = 6;
        else gear = 4;
    // Gear N
    } else {
        gear = 0;  
    }

    return gear;
}
