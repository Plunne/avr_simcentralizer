#ifndef _SETUP_H
#define _SETUP_H

#include <avr/io.h>
#include <util/delay.h>

#include "AnalogDigital.h"
#include "Gear.h"

/*******************
 * Initialisations *
 *******************/

void InitRegisters(void); // Init registers

/*************
 * Functions *
 *************/

#define TX_LED      PD5
#define RX_LED      PB0

// Axis limits
#define AXIS_MINIMUM         0       // Negative Axis min value
#define AXIS_CENTER_NEG      420     // Negative Axis max value
#define AXIS_CENTER_POS      600     // Positive Axis min value
#define AXIS_MAXIMUM         1000    // Positive Axis max value

// Shifter
#define SHIFTER_X   PF7
#define SHIFTER_Y   PF6

// Handbrake
#define HANDBRAKE   PF5

// Display
#define ANODE_A     PD1
#define ANODE_B     PD0
#define ANODE_C     PD4
#define ANODE_D     PC6
#define ANODE_E     PD7
#define ANODE_F     PE6
#define ANODE_G     PB4

#endif // _SETUP_H
