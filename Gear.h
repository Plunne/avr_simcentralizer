#ifndef _GEAR_H
#define _GEAR_H

#include <string.h>

#include "Setup.h"

unsigned char getGear(void);
void displayGear(unsigned char g);

#endif // _SHIFTER_H
