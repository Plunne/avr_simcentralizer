#include "Gear.h"

void displayGear(unsigned char g) {

    char tabGear[] = {0,0,0,0,0,0,0};

    switch(g)
    {
        case 0:
            strcpy(tabGear,"1111110");
            break;
        case 1:
            strcpy(tabGear,"0110000");
            break;
        case 2:
            strcpy(tabGear,"1101101");
            break;
        case 3:
            strcpy(tabGear,"1111001");
            break;
        case 4:
            strcpy(tabGear,"0110011");
            break;
        case 5:
            strcpy(tabGear,"1011011");
            break;
        case 6:
            strcpy(tabGear,"1011111");
            break;
        default:
            break;
    }

    // Char -> Int
    for(int i=0; i<7; i++) tabGear[i] -= '0';

    // 7 seg Display
    digitalWrite(&PORTD, ANODE_A, tabGear[0]);
    digitalWrite(&PORTD, ANODE_B, tabGear[1]);
    digitalWrite(&PORTD, ANODE_C, tabGear[2]);
    digitalWrite(&PORTC, ANODE_D, tabGear[3]);
    digitalWrite(&PORTD, ANODE_E, tabGear[4]);
    digitalWrite(&PORTE, ANODE_F, tabGear[5]);
    digitalWrite(&PORTB, ANODE_G, tabGear[6]);
}
